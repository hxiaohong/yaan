# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140322135415) do

  create_table "admins", :force => true do |t|
    t.string   "recsts",     :limit => 1,   :default => "0"
    t.string   "uid",        :limit => 20
    t.string   "pwd",        :limit => 100
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  create_table "docs", :force => true do |t|
    t.string   "recsts",     :limit => 1,   :default => "0"
    t.string   "title",      :limit => 100
    t.string   "val1"
    t.string   "val2"
    t.string   "val3"
    t.integer  "order",                     :default => 0
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  create_table "posts", :force => true do |t|
    t.string   "recsts",     :limit => 1,   :default => "0"
    t.string   "title",      :limit => 100
    t.text     "content"
    t.string   "typecd",     :limit => 20
    t.string   "isreco",     :limit => 1,   :default => "0"
    t.integer  "visit",                     :default => 0
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  create_table "refcds", :force => true do |t|
    t.string   "recsts",     :limit => 1,    :default => "0"
    t.string   "grpcd",      :limit => 20
    t.string   "cd",         :limit => 20
    t.string   "sdesc",      :limit => 200
    t.string   "fdesc",      :limit => 1000
    t.string   "val1"
    t.string   "val2"
    t.string   "val3"
    t.string   "ownto",      :limit => 20,   :default => ""
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

end
