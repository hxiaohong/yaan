class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins,:options => 'CHARSET=utf8' do |t|
      t.string :recsts,:limit=>1,:default=>0 
      t.string :uid,:limit=>20
      t.string :pwd,:limit=>100
      t.timestamps
    end
  end
end
