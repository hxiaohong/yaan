#encoding: utf-8 
class CreateRefcds < ActiveRecord::Migration
  def change
    create_table :refcds,:options => 'CHARSET=utf8' do |t|
      t.string :recsts,:limit=>1,:default=>0 
      t.string :grpcd,:limit=>20
      t.string :cd,:limit=>20
      t.string :sdesc,:limit=>200
      t.string :fdesc,:limit=>1000
      t.string :val1
      t.string :val2
      t.string :val3
      t.string :ownto,:limit=>20,:default=>''
      t.string :created_by
      t.string :updated_by
      t.timestamps
    end 
  end
end
