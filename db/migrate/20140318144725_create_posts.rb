#encoding: utf-8 
class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts,:options => 'CHARSET=utf8' do |t|
      t.string :recsts,:limit=>1,:default=>0 
      t.string :title,:limit=>100
      t.text :content 
      t.string :typecd,:limit=>20
      t.string :val1,:limit=>100
      t.string :val2,:limit=>100
      t.string :isreco,:limit=>1,:default=>0  #是否推荐 
      t.integer :visit,:default=>0 
      t.timestamps
    end
  end
end
