Company::Application.routes.draw do

  captcha_route
    get "main/home" 
    get "news" => "main#news"
    get "dynamic" => "main#dynamic"
    get "intro" => "main#intro"
    get "policy" => "main#policy"
    get "supply" => "main#supply"
    get "product" => "main#product"
    get "tech" => "main#tech"
    get 'detail/:id' => "main#detail"
    get 'pro_detail/:id' => "main#pro_detail"

    post "main/post_list"
    post "main/post_count"
    post "main/inc_visit"
    get "main/search"



    get "admin" => 'admin#home'
    get "admin/post_new"
    get "admin/post_edit/:id" => "admin#post_edit"
    post "admin/post_update"
    get "admin/post/:typecd" =>"admin#post"
    post "admin/post_save"
    post "admin/post_list"
    post "admin/post_remove"
    get "admin/login"
    post "admin/check_login"
    get "admin/logout"
    get "admin/home"
    get "admin/pwd"
    get "admin/link"
    get "admin/link_new"
    get "admin/link_list"
    post "admin/link_remove"
    post "ueditor/imageUp"
    post "ueditor/fileUp"
    get 'admin/link_edit/:id' => "admin#link_edit"
    get "admin/product_new"
    get "admin/product"
    get "admin/product_edit/:id" => "admin#product_edit"
    post "admin/product_list"
    post "admin/up_pic"


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'main#home'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
