function my_template(data,template,$contaniner,startindex){ 
   $contaniner.empty();
   var reg;
   _.each(data, function(item,index){
     var temp=template.html();
     _.each(item,function(val,key){
        reg=new RegExp("@{"+key+"}","gi");
        temp=temp.replace(reg,val);
        reg=new RegExp("@{index}","gi");
        temp=temp.replace(reg,startindex+index+1);
        reg=new RegExp("@{0index}","gi");
        temp=temp.replace(reg,leftpad(startindex+index+1));
     }); 
     $contaniner.append(temp);       
   }); 
}

function  get_post(typecd,size){
  $("#list_post_con").empty();
  $("#pagenation").empty();
  $.post('/main/post_count',{typecd:typecd},function(data){
    if(data.count < 1){
       return;
    } 
    $("#pagenation").pagination(data.count, {
      items_per_page:size,
      current_page: 0,
      link_to: "###",
      callback: function(page){
        get_list(typecd,page,size);
      }
    });  
  }); 
} 

function  get_list(typecd,page,size){
  $.post("/main/post_list",{typecd:typecd,page:page,size:size},function(data){
    my_template(data.post,$('#temp_post'),$("#list_post_con"),0);
  });
}

function leftpad(num)
{
   return num<10 ? "0" + num : num;
}

// JavaScript Document
//  加入收藏 <a onclick="AddFavorite(window.location,document.title)">加入收藏</a>

function AddFavorite(sURL, sTitle)
{
    try
    {
        window.external.addFavorite(sURL, sTitle);
    }
    catch (e)
    {
        try
        {
            window.sidebar.addPanel(sTitle, sURL, "");
        }
        catch (e)
        {
            alert("加入收藏失败，请使用Ctrl+D进行添加");
        }
    }
}
//设为首页 <a onclick="SetHome(this,window.location)">设为首页</a>
function SetHome(obj,vrl){
   try{
      obj.style.behavior='url(#default#homepage)';obj.setHomePage(vrl);
   }
   catch(e){
     if(window.netscape) {
             try {
                     netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
             }
             catch (e) {
                     alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。");
             }
             var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
             prefs.setCharPref('browser.startup.homepage',vrl);
      }
   }
}