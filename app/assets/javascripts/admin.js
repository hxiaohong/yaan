function add_tab(title, url,icon)
{
  if($('#tabs').tabs('exists',title)){
    $('#tabs').tabs('select',title);
    return false;
  }
  $('#tabs').tabs('add',{
    title: title,
    selected: true,
    content:'<iframe src="'+url+'" scrolling="auto" frameborder="0" style="width: 100%; height: 98%; overflow: hidden"></iframe>',
    closable:true, 
    /*tools:[{
      iconCls:icon,
      handler:function(){
          alert('refresh');
      }
    }]*/
  });
}

function show_msg(title,msg){
  $.messager.show({
    title:title,
    msg:msg
  });
}

function slide(con){
  $.messager.show({
    title:'温馨提示',
    msg:con,
    timeout:5000,
    showType:'slide'
  });
 }

function alert_info(msg) {
   $.messager.alert('操作成功',msg,'info');
 }
function alert_error(msg) {
   $.messager.alert('操作错误',msg,'error');
 }