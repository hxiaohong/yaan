class Post < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_protected
  def self.typecd_used
  	_all_types = Refcd.current('POSTTYP')
    _owntos = _all_types.map{|m|m.ownto}
    _all_types.select{|m|_owntos.index(m.cd).nil?}.each do |m|
       m[:full_sdesc] = m.sdesc
       m[:full_sdesc] = (Refcd.key('POSTTYP',m.ownto).sdesc + ' > ' + m.sdesc) if !m.ownto.nil? && m.ownto!=''
    end
  end
  def self.children_type(cd)
    _all_types = Refcd.current('POSTTYP')
    _all_types.select{|m| m.ownto == cd}
  end
  
end
