class Refcd < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_protected
  @@CACHE_KEY="REFCD"
  def self.current(grpcd) 
    Rails.cache.write(@@CACHE_KEY, Refcd.all) if Rails.cache.read(@@CACHE_KEY).nil? 
    Rails.cache.read(@@CACHE_KEY).find_all{|r|r.grpcd==grpcd}
  end 
  def self.key(grpcd,cd)
    Rails.cache.write(@@CACHE_KEY, Refcd.all) if Rails.cache.read(@@CACHE_KEY).nil? 
    Rails.cache.read(@@CACHE_KEY).find_all{|r|r.grpcd==grpcd && r.cd==cd}[0]
  end
  def self.update
     Rails.cache.write(@@CACHE_KEY, Refcd.all)
  end
end
