#encoding: utf-8
class AdminController < ApplicationController 
  before_filter :is_login,:only=>['home']
  protect_from_forgery
  def post_list_field
    'id,title,typecd,created_at,val1,visit,isreco'
  end
  def login
    render layout:false 
  end
  def check_login
    if !Admin.where({:user_id=>params[:uid],:pwd=>Digest::MD5.hexdigest(params[:pwd])})[0].nil?
      session[:mem]= 'admin'
      redirect_to  '/admin/home'
    else
      redirect_to  '/admin/login' 
    end
  end
  def logout
    session[:mem]= nil
    redirect_to  '/admin/login' 
  end
  def home
    render layout:false
  end
  def pwd
    
  end
  def post_new
  	 
  end
  def post_save
    _datas = package(params,Post.attribute_names)
    Post.create(_datas)
    render json: {status:true}
  
  end
  def post_edit
    @post = Post.find(params[:id].to_i)
  end
  def post_update
    _datas = package(params,Post.attribute_names)
    _datas[:isreco] = '0' if _datas[:isreco]!='1'
    _post = Post.find(params[:id].to_i)
    _post.update_attributes(_datas)
    render json: {status:true}
  rescue
    render json: {status:false}
  end
  def post
    
  end
  def post_list
    _page = params[:page].to_i
    _rows = params[:rows].to_i 
    _typecd = params[:typecd]
    _posts = Post.where({:typecd=>_typecd}).select(post_list_field).offset((_page - 1) * _rows).limit(_rows).order('id DESC').each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d %H:%M:%S')
    end

    render json:{rows: _posts,total:Post.where({:typecd=>_typecd}).count}
  end

  def post_remove
    Post.find(params[:ids].split(',').map{|x|x.to_i}).each{|p|p.destroy}
    render json:{status:true}
  rescue
    render json:{status:false}
  end

  def link
    
  end
  
  def link_list
    _page = params[:page].to_i
    _rows = params[:rows].to_i 
    _posts = Post.where({:typecd=>'LINK'}).select(post_list_field).offset((_page - 1) * _rows).order('id DESC').limit(_rows)
    render json:{rows: _posts,total:Post.where({:typecd=>'LINK'}).count}
  end
  def link_edit
    @link = Post.find(params[:id].to_i)
  end
  def product_new
    
  end
  def product
    
  end
  def product_list
    _page = params[:page].to_i
    _rows = params[:rows].to_i 
    _posts = Post.where({:typecd=>'PRODUCT'}).select(post_list_field).offset((_page - 1) * _rows).limit(_rows).order('id DESC').each do |p|
       p[:val1] = p[:val1].split('.')[0] + "_s." + p[:val1].split('.')[1] 
       p[:insert_time] =p.created_at.strftime('%Y-%m-%d %H:%M:%S') 
    end
    render json:{rows: _posts,total:Post.where({:typecd=>'PRODUCT'}).count}
  end
  def product_edit
    @product = Post.find(params[:id].to_i)
  end
  def up_pic  
    _file = params[:Filedata]
    _file_name = "#{Time.now.strftime("%y%m%d%H%M%S")}#{rand(99).to_s}.#{_file.original_filename.split('.')[1]}" 
    _small_file_name = _file_name.split('.')[0] + "_s." + _file_name.split('.')[1]  
    upload_file(_file,_small_file_name,"product","160x160!")
    upload_file(_file,_file_name,"product",nil)
    render text: _file_name and return 
  rescue
    render text: "0"
  end
  def upload_file(file,filename,folder,size) 
      image = MiniMagick::Image.read(file)
      image.style  'Normal'
      image.resize size if !size.nil?
      image.write  "#{Rails.root}/public/upload/#{folder}/#{filename}" 
      return filename  
  end 
end
