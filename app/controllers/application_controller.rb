class ApplicationController < ActionController::Base
  protect_from_forgery 
  def is_login
  	if session[:mem].nil?
      redirect_to  '/admin/login' and return
    end
  end
  def package(params,attributes)
    #Hash[params.select{|k,| attributes.include? k}]
    params.reject{|k,| not attributes.include? k}
  end 

end
