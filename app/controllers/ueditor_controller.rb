class UeditorController < ApplicationController
	def imageUp
	  require 'fileutils' 
	  _tmp = params[:upfile]
	  _file_path = _big_name = "/upload/ueditor/images/#{Time.now.strftime("%y%m%d%H%M%S")}.#{_tmp.original_filename.split('.')[1]}"
	  FileUtils.cp _tmp.path, "public"+_file_path 
	  FileUtils.chmod 0755,"public"+_file_path 
	  render json: {:url=>_file_path,:title=>_tmp.original_filename,:original=>_tmp.original_filename,:state=>'SUCCESS'}
	end


	def fileUp
	  require 'fileutils' 
	  _tmp = params[:upfile]
	  _file_path = _big_name = "/upload/ueditor/files/#{Time.now.strftime("%y%m%d%H%M%S")}.#{_tmp.original_filename.split('.')[1]}"
	  FileUtils.cp _tmp.path, "public"+_file_path 
	  FileUtils.chmod 0755,"public"+_file_path  
	  render json: {:url=>_file_path,:fileType=>_tmp.original_filename.split('.')[1],:original=>_tmp.original_filename,:state=>'SUCCESS'}
	end
end
