class MainController < ApplicationController
  def post_list_field
    'id,title,typecd,created_at,val1,visit,isreco'
  end
  def home
    @list = {
      :ZIXUN=>latest('ZIXUN',5),
      :DYNAMIC=>latest('DYNAMIC',5),
      :GDZC=>latest('GDZC',6),
      :FLFG=>latest('FLFG',6),
      :XZTZ=>latest('XZTZ',6), 
      :SUPPLY=>latest('SUPPLY',8),
      :TECH=>latest('TECH',8),
      :PRODUCT =>latest('PRODUCT',15),
    }
    @menu = 0
  end
  def news  
    @hots = hots('ZIXUN',5)
    @reco = reco('ZIXUN',5)
    @menu = 1
  end
  def dynamic
  	@hots = hots('DYNAMIC',5)
    @reco = reco('DYNAMIC',5)
    @menu = 2
  end
  def intro
  	@menu = 3
  end
  def policy
  	@hots = hots('POLICY',5)
    @reco = reco('POLICY',5) 
    @menu = 4
  end
  def supply
  	@hots = hots('SUPPLY',5)
    @reco = reco('SUPPLY',5)
    @menu = 5
  end
  def product
    @hots = hots('PRODUCT',5)
    @reco = reco('PRODUCT',5)
    @menu = 6
  end
  def tech
  	@hots = hots('TECH',5)
    @reco = reco('TECH',5)
    @menu = 7
  end
  def detail
    @post = Post.find(params[:id].to_i)
  end
  def pro_detail
    @post = Post.find(params[:id].to_i)
  end
  
  def post_list
    _typecd = params[:typecd]
    _page = params[:page].nil?? 1 : params[:page].to_i
    _size = params[:size].nil?? 10 : params[:size].to_i
    _posts = Post.where({:typecd=>all_type(_typecd)}).select(post_list_field).offset(_page * _size).limit(_size).order('id DESC').each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d')
      p[:val1] = p[:val1].split('.')[0] + "_s." + p[:val1].split('.')[1]  if p[:typecd] == 'PRODUCT'
    end
    render json:{post:_posts}
  end
  def post_count
     _count = Post.where({:typecd=>all_type(params[:typecd])}).count
     render json:{count:_count}
  end


  def latest(typecd,count)
    Post.where({:typecd=>all_type(typecd)}).select(post_list_field).limit(count).order('id DESC').all.each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d')
      p[:val1] = p[:val1].split('.')[0] + "_s." + p[:val1].split('.')[1] if p[:typecd] == 'PRODUCT'
    end
  end
 
  def hots(typecd,count)
    Post.where({:typecd=>all_type(typecd)}).select(post_list_field).limit(count).order('visit DESC').all.each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d')
      p[:val1] = p[:val1].split('.')[0] + "_s." + p[:val1].split('.')[1] if p[:typecd] == 'PRODUCT'
    end
  end

  def reco(typecd,count)
    Post.where({:typecd=>all_type(typecd),:isreco=>'1'}).select(post_list_field).limit(count).order('id DESC').all.each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d')
    end
  end

  def all_type(typecd)
    Post.children_type(typecd).map{|m|m.cd}.push(typecd)
  end
  def inc_visit
    _post = Post.find(params[:id].to_i)
    _post.visit += 1
    _post.save
  end

  def search
    _keyword = params[:keyword]
    @list = Post.where(["title like  ? and typecd in (?)", "%#{_keyword}%",Refcd.current('POSTTYP').map{|r|r.cd}]).select('id,title,created_at,typecd').order('id DESC').all.each do |p|
      p[:insert_time] =p.created_at.strftime('%Y-%m-%d') 
    end
    @list = [] if _keyword == ''
  end
end